import java.util.Scanner;

import foldertools.ToolCombineFiles;

public class CommandMain {

	
	/**
	 *  This class is for generating runnable jar for combining files
	 * 
	 */
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Please enter the root path:");
		String rootPath = "";
		if(scanner.hasNext()){
			rootPath = scanner.nextLine();
		}
		
		System.out.println("Please enter the destination path:");
		String destPath = "";
		if(scanner.hasNext()){
			destPath = scanner.nextLine();
		}
		
		scanner.close();
		
		if(rootPath.isEmpty() || destPath.isEmpty()){
			System.out.println("Root path or destination path is empty");
			return;
		}
		
		ToolCombineFiles tool_combineFiles = new ToolCombineFiles();
		tool_combineFiles.combine(rootPath, destPath);
		
		System.out.println("Combination is finish");
	}

}
