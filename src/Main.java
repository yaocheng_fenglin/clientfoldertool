import java.util.Scanner;

import org.json.JSONObject;

import foldertools.CombineFiles;
import foldertools.ToolCombineFiles;
import foldertools.ToolCombineFilesReader;

public class Main {

	public static void main(String[] args) {
		/*String rootPath = "D:\\testProgram\\testProgram1\\auto_index"; 
		String destPath = "D:\\testProgram\\testProgram2\\yy__1.fenglincf";
		String destPath_2 = "D:\\testProgram\\testProgram2\\yy__2.fenglincf";*/
		
		/*
		//MacBook
		String rootPath = "/Users/admin/yctest/testProgram1/auto_index/doc"; 
		String destPath = "/Users/admin/yctest/destadd/doc.fenglincf";
		
		String rootPath_2 = "/Users/admin/yctest/testProgram1/auto_index/src"; 
		String destPath_2 = "/Users/admin/yctest/destadd/src.fenglincf";
		
		String rootPath_3 = "/Users/admin/yctest/testProgram1/auto_index/test"; 
		String destPath_3 = "/Users/admin/yctest/destadd/test.fenglincf";
		*/
		
		/*String rootPath = "D:\\testProgram\\testProgram1\\auto_index\\doc"; 
		String destPath = "D:\\testProgram\\testProgram2\\doc.fenglincf";
		
		String rootPath_2 = "D:\\testProgram\\testProgram1\\auto_index\\src"; 
		String destPath_2 = "D:\\testProgram\\testProgram2\\src.fenglincf";
		
		String rootPath_3 = "D:\\testProgram\\testProgram1\\auto_index\\test"; 
		String destPath_3 = "D:\\testProgram\\testProgram2\\test.fenglincf";
		
		ToolCombineFiles tool_combineFiles = new ToolCombineFiles();
		tool_combineFiles.combine(rootPath, destPath);
		tool_combineFiles.combine(rootPath_2, destPath_2);
		tool_combineFiles.combine(rootPath_3, destPath_3);*/
		
		
		//TODO test read multi files
		//String combineFilePath = "D:\\testProgram\\testProgram2\\yy__1.fenglincf";
		//String subFilePath = "D:\\testProgram\\testProgram1\\auto_index\\src\\CTest.cpp";
		//String subFilePath = "D:\\testProgram\\testProgram1\\auto_index\\doc\\students_t_eg_3.png";
		
		//MACBOOK
		/*
		String combineFilePath = "/Users/admin/yctest/destadd";
		String subFilePath = "/Users/admin/yctest/testProgram1/auto_index/src/CTest.cpp";
		*/
		
		//String combineFilePath = "D:\\testProgram\\testProgram2";
		//String combineFilePath = "D:\\testProgram\\testProgram2\\src.fenglincf";
		//String subFilePath = "D:\\testProgram\\testProgram1\\auto_index\\src\\CTest.cpp";
		
		String subFilePath = ".\\TestMaterial\\testProgram1\\auto_index\\src\\CTest.cpp";
		String combineFilePath = ".\\TestMaterial\\testProgram2\\test02.fenglincf";
		ToolCombineFilesReader reader = new ToolCombineFilesReader();
		reader.init_load_byFile(combineFilePath);
		//reader.init_load_byDirectory(combineFilePath);
		
		//byte[] readResult = reader.readWholeFile(subFilePath);
		//System.out.println(new String(readResult));
		
		//String destFilePath = "D:\\testProgram\\testExport_333ii.cpp";
		//reader.exportWholeFile(subFilePath, destFilePath);
		//byte[] readResult = reader.readWholeFile(subFilePath);
		//System.out.println(new String(readResult));
		
		byte[] readResult = reader.readByRange(subFilePath, 83, 20, false);
		System.out.println(new String(readResult));
		byte[] readResult2 = reader.readByRange(subFilePath, 0, 20, true);
		System.out.println(new String(readResult2));
		
		
		reader.close_global_raf();
		//TODO verify combine files should by order asc
	}

}
