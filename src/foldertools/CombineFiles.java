package foldertools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import foldertools.utils.Const;


public class CombineFiles {

	private int fileNum;
	private long totalPosition;
	private List<SubFile> subFileList;
	private JSONObject jsonObj;
	
	public CombineFiles(){
		
	}
	
	public void combine(String rootPath, String destPath){
		File destFile = new File(destPath);
		if(destFile.exists()){
			System.out.println("Destination file already exists, path:" + destPath);
			return;
		}else{
			try {
				destFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}
		
		this.init(rootPath);
		
		FileOutputStream fo = null;
		FileChannel destFileChannel = null;
		try{
			fo = new FileOutputStream(destFile);
			destFileChannel = fo.getChannel();
			
			this.insertIndexToNewFile(destFileChannel);
			this.insertFilesToNewFile(destFileChannel);
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(fo != null){
					fo.close();
				}
				if(destFileChannel != null){
					destFileChannel.close();
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	private void init(String rootPath){
		this.fileNum = 0;
		this.totalPosition = 0;
		this.subFileList = new ArrayList<SubFile>();
		this.jsonObj = new JSONObject();
		
		File rootFile = new File(rootPath);
		this.getSubFiles(rootFile);
	}
	
	private void getSubFiles(File file){
		File[] curFiles = file.listFiles();
		for(File fileObj : curFiles){
			if(fileObj.isDirectory()){
				this.getSubFiles(fileObj);
			}else{
				this.fileNum++;
				SubFile subFile = new SubFile(this.fileNum, fileObj);
				this.subFileList.add(subFile);
				
				JSONObject jsonDetail = new JSONObject();
				jsonDetail.put(Const.JSONKEY_ID, subFile.getId());
				jsonDetail.put(Const.JSONKEY_POSITION, this.totalPosition);
				jsonDetail.put(Const.JSONKEY_FILE_LEN, fileObj.length());
				this.jsonObj.put(fileObj.getPath(), jsonDetail.toString());
				
				this.totalPosition =  this.totalPosition + fileObj.length() + subFile.getAppend_nul_num() + Const.HBDOC_HARDCODE_LEN;
			}
		}
	}
	
	
	private void insertIndexToNewFile(FileChannel destFileChannel){
		String jsonStr = this.jsonObj.toString();
		byte[] jsonByteArray = null;
		try {
			jsonByteArray = jsonStr.getBytes(Const.ENCODE_METHOD);
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		
		
		//Calculate the number of NUL to append to index
		int append_nul_num = 0;
		int remainderNum = jsonByteArray.length%8;
		int appendNum = 8 - remainderNum;
		if(appendNum > 0 && appendNum < 8){
			append_nul_num = appendNum;
		}else{
			append_nul_num = 0;
		}
		byte[] bArray_nul = new byte[append_nul_num];
		
		//insert header
		ByteBuffer buffer = ByteBuffer.allocate(jsonByteArray.length + Const.HB_HARDCODE_LEN + append_nul_num);
		buffer.put(Const.SOH);
		buffer.put(Const.HB);
		buffer.put(Const.NUL_);
		buffer.putInt(jsonByteArray.length);
		buffer.putInt(append_nul_num);
		buffer.put(Const.NUL_);
		buffer.put(Const.NUL_);
		buffer.put(Const.NUL_);
		buffer.put(Const.STX);
		
		//insert main body
		buffer.put(jsonByteArray);
		
		//insert nul appended
		buffer.put(bArray_nul);
		
		//insert tailer
		buffer.put(Const.HBEND_ALL);
		
		buffer.flip();
		try {
			destFileChannel.write(buffer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	private void insertFilesToNewFile(FileChannel destFileChannel){
		for(SubFile subFile : this.subFileList){
			this.insertFileHeader(subFile, destFileChannel);
			this.insertFileMainBody(subFile, destFileChannel);
			this.insertFileTailer(destFileChannel);
		}
		
	}
	
	private void insertFileHeader(SubFile subFile, FileChannel destFileChannel){
		ByteBuffer buffer = ByteBuffer.allocate(Const.HBDOC_HARDCODE_LEN_HEADER);
		buffer.put(Const.SOH);
		buffer.put(Const.HBDOC);
		buffer.put(Const.NUL_);
		buffer.put(Const.NUL_);
		buffer.putInt(subFile.getId());
		buffer.put(Const.NUL_);
		buffer.put(Const.NUL_);
		buffer.put(Const.NUL_);
		buffer.put(Const.STX);
		buffer.flip();
		try {
			destFileChannel.write(buffer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void insertFileMainBody(SubFile subFile, FileChannel destFileChannel){
		FileInputStream sourceInputStream = null;
		FileChannel sourceFileChannel = null;
		try{
			File file = subFile.getFile();
			sourceInputStream = new FileInputStream(file);
			sourceFileChannel = sourceInputStream.getChannel();
			sourceFileChannel.transferTo(0, file.length(), destFileChannel);
			
			if(subFile.getAppend_nul_num() > 0){
				ByteBuffer buffer_nul = ByteBuffer.allocate(subFile.getAppend_nul_num());
				destFileChannel.write(buffer_nul);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(sourceInputStream != null){
					sourceInputStream.close();
				}
				if(sourceFileChannel != null){
					sourceFileChannel.close();
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	private void insertFileTailer(FileChannel destFileChannel){
		ByteBuffer buffer = ByteBuffer.allocate(Const.HBDOC_HARDCODE_LEN_TAILER);
		buffer.put(Const.DOCEND_ALL);
		buffer.flip();
		try {
			destFileChannel.write(buffer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private class SubFile {
		private int id;
		private File file;
		private int append_nul_num;
		
		public SubFile(int id, File file) {
			this.id = id;
			this.file = file;
			
			long remainderNum = file.length()%8;
			long appendNum = 8 - remainderNum;
			if(appendNum > 0 && appendNum < 8){
				this.append_nul_num = (int) appendNum;
			}else{
				this.append_nul_num = 0;
			}
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public File getFile() {
			return file;
		}

		public void setFile(File file) {
			this.file = file;
		}

		public int getAppend_nul_num() {
			return append_nul_num;
		}

		public void setAppend_nul_num(int append_nul_num) {
			this.append_nul_num = append_nul_num;
		}

	}
	
}
