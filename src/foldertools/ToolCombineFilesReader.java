package foldertools;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import foldertools.utils.Const;

public class ToolCombineFilesReader {
	
	private Map<String, JSONObject> global_json_map;
	private Map<String, RandomAccessFile> global_raf_map;
	
	public ToolCombineFilesReader(){
		this.global_json_map = new HashMap<String, JSONObject>();
		this.global_raf_map = new HashMap<String, RandomAccessFile>();
	}
	
	/**
	 * Call this method before reading
	 */
	public void init_load_byFile(String combineFilePath){
		File file = new File(combineFilePath);
		
		if(!file.exists()){
			System.out.println("No such a file:" + combineFilePath);
			return;
		}
		
		if(file.isDirectory()){
			System.out.println("It is directory:" + combineFilePath);
			return;
		}
		
		if(!this.isCorrectPostfix(file.getName())){
			System.out.println("Wrong postfix:" + combineFilePath);
			return;
		}
		
		List<File> combineFileList = new ArrayList<File>();
		combineFileList.add(file);
		
		this.load_implement(combineFileList);
	}
	
	/**
	 * Call this method before reading
	 */
	public void init_load_byDirectory(String rootPath){
		File rootFile = new File(rootPath);
		
		if(!rootFile.isDirectory()){
			return;
		}
		
		List<File> combineFileList = new ArrayList<File>();
		
		this.getSubFiles(rootFile, combineFileList);
		
		this.load_implement(combineFileList);
	}
	
	private void getSubFiles(File file, List<File> combineFileList){
		File[] curFiles = file.listFiles();
		for(File fileObj : curFiles){
			if(fileObj.isDirectory()){
				this.getSubFiles(fileObj, combineFileList);
			}else{
				if(this.isCorrectPostfix(fileObj.getName())){
					combineFileList.add(fileObj);
				}
			}
		}
	}
	
	private boolean isCorrectPostfix(String fileName){
		if(fileName.lastIndexOf(".") == -1){
			return false;
		}
		
		String postfix = fileName.substring(fileName.lastIndexOf(".") + 1);
		if(Const.COMBINEFILE_POSTFIX.equals(postfix)){
			return true;
		}
		
		return false;
	}
	
	private void load_implement(List<File> combineFileList){
		for(File combineFile : combineFileList){
			this.load_each_combineFile(combineFile);
		}
	}
	
	private void load_each_combineFile(File combineFile){
		RandomAccessFile raf = null;
		JSONObject jsonIndexObj = null;
		try {
			raf = new RandomAccessFile(combineFile, "r");
			
			String combineFilePath = combineFile.getPath();
			
			byte b_SOH = raf.readByte();
			if(Const.SOH != b_SOH){
				throw new Exception(String.format("Error, wrong format, file path: %s", combineFilePath));
			}
			
			byte[] b_HB = new byte[2];
			raf.read(b_HB, 0, 2);
			if(!Arrays.equals(b_HB, Const.HB)){
				throw new Exception(String.format("Error, wrong format, file path: %s", combineFilePath));
			}
			
			raf.read();
			
			int indexLen = raf.readInt();
			
			int index_nul_appended = raf.readInt();
			
			raf.read();
			raf.read();
			raf.read();
			
			byte b_STX = raf.readByte();
			if(b_STX != Const.STX) {
				throw new Exception(String.format("Error, wrong format, file path: %s", combineFilePath));
			}
			
			byte[] bArray_index = new byte[indexLen];
			raf.read(bArray_index, 0, indexLen);
			
			byte[] bArray_nul = new byte[index_nul_appended];
			raf.read(bArray_nul, 0, index_nul_appended);
			
			byte[] b_indexTailer = new byte[Const.HB_HARDCODE_LEN_TAILER];
			raf.read(b_indexTailer, 0, Const.HB_HARDCODE_LEN_TAILER);
			
			if(!Arrays.equals(b_indexTailer, Const.HBEND_ALL)){
				throw new Exception(String.format("Error, wrong format, file path: %s", combineFilePath));
			}
			
			String indexStr = new String(bArray_index, Const.ENCODE_METHOD);
			jsonIndexObj = new JSONObject(indexStr);
			
			int totalIndexLen = indexLen + Const.HB_HARDCODE_LEN + index_nul_appended;
			
			
			for(String subFilePath : jsonIndexObj.keySet()){
				String detailStr =  jsonIndexObj.getString(subFilePath);
				JSONObject subFileJsonDetail = new JSONObject(detailStr);
				
				long position = subFileJsonDetail.getLong(Const.JSONKEY_POSITION);
				long actualPosition = totalIndexLen + position + Const.HBDOC_HARDCODE_LEN_HEADER;
				subFileJsonDetail.put(Const.JSONKEY_ACTUAL_POSITION, actualPosition);
				
				long fileLen = subFileJsonDetail.getLong(Const.JSONKEY_FILE_LEN);
				long actualPosition_end = actualPosition + fileLen;
				subFileJsonDetail.put(Const.JSONKEY_ACTUAL_POSITION_END, actualPosition_end);
				
				subFileJsonDetail.put(Const.JSONKEY_RAF_SUBFILE_POINTER, actualPosition);
				
				subFileJsonDetail.put(Const.JSONKEY_COMBINEFILE_PATH, combineFilePath);
				
				global_json_map.put(subFilePath, subFileJsonDetail);
			}
			
			this.global_raf_map.put(combineFilePath, raf);
		} catch (Exception e) {
			try {
				if(raf != null){
					raf.close();
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			return;
		}
	}
	
	private byte[] readFile(String subFilePath, long positionFrom, int readLen, boolean isReadWholeFile, boolean isContinueRead){
		byte[] resultByteArray = null;
		
		if(subFilePath == null){
			System.out.println("Sub file path is null");
			return null;
		}
		
		JSONObject subFileJsonDetail = this.global_json_map.get(subFilePath);
		if(subFileJsonDetail == null){
			System.out.println(String.format("File not exists in combine files, file:%s", subFilePath));
			return null;
		}
		
		String combineFilePath = subFileJsonDetail.getString(Const.JSONKEY_COMBINEFILE_PATH);
		
		RandomAccessFile raf = this.global_raf_map.get(combineFilePath);
		
		synchronized (raf) {
			try {
				resultByteArray = readFileImplement(raf, positionFrom, readLen, isReadWholeFile, subFileJsonDetail, isContinueRead);
			} catch (Exception e) {
				try {
					if(raf != null){
						raf.close();
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
				
				try {
					File combineFile = new File(combineFilePath); 
					raf = new RandomAccessFile(combineFile, "r");
					this.global_raf_map.put(combineFilePath, raf);
				} catch (FileNotFoundException e2) {
					e2.printStackTrace();
				}
				return null;
			}
		}
		
		return resultByteArray;
	}
	
	public byte[] readWholeFile(String subFilePath){
		return this.readFile(subFilePath, 0, 0, true, false);
	}
	
	public byte[] readByRange(String subFilePath, long positionFrom, int readLen, boolean isContinueRead){
		return this.readFile(subFilePath, positionFrom, readLen, false, isContinueRead);
	}
	
	
	/**
	 * Use for test
	 */
	public void exportWholeFile(String subFilePath, String destFilePath){
		byte[] file_bArray = this.readWholeFile(subFilePath);
		
		File destFile = new File(destFilePath);
		if(destFile.exists()){
			System.out.println("Already exists");
			return;
		}else{
			try {
				destFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}
		
		BufferedOutputStream bos = null;
		try{
			bos = new BufferedOutputStream(new FileOutputStream(destFile));
			bos.write(file_bArray);
		}catch(Exception e){
			e.printStackTrace();
		} finally{
			if(bos != null){
				try{
					bos.flush();
					bos.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		
	}
	
	
	/**
	 * 
	 * @param positionFrom position offset from current sub file, related to the logged position last time reading
	 * @param isContinueRead read the sub file start from last time logged position, or start reading from the file head
	 * @throws Exception
	 * 
	 */
	private byte[] readFileImplement(RandomAccessFile raf, long positionFrom, int readLen, boolean isReadWholeFile, 
			JSONObject subFileJsonDetail, boolean isContinueRead) throws Exception{
		long jumpToPosition = 0;
		
		if(isContinueRead){
			jumpToPosition = subFileJsonDetail.getLong(Const.JSONKEY_RAF_SUBFILE_POINTER);
		}else{
			jumpToPosition = subFileJsonDetail.getLong(Const.JSONKEY_ACTUAL_POSITION);
		}
		
		jumpToPosition += positionFrom;
		raf.seek(jumpToPosition);
		
		if(isReadWholeFile){
			readLen = (int)subFileJsonDetail.get(Const.JSONKEY_FILE_LEN);
		}
		
		byte[] bArray_fileMainBody = new byte[readLen];
		raf.read(bArray_fileMainBody, 0, readLen);
		
		long curPosition = raf.getFilePointer();
		
		long fileEndPosition = subFileJsonDetail.getLong(Const.JSONKEY_ACTUAL_POSITION_END);
		if(curPosition > fileEndPosition){
			System.out.println("Currently reading Exceed the sub file size");
			return null;
		}
		
		subFileJsonDetail.put(Const.JSONKEY_RAF_SUBFILE_POINTER, curPosition);
		
		return bArray_fileMainBody;
	}
	
	/**
	 * 	Once quit this reader, call this method to close all RandomAccessFile objects' IO streams
	 */
	public void close_global_raf(){
		for(RandomAccessFile raf: this.global_raf_map.values()){
			try {
				if(raf != null){
					raf.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
