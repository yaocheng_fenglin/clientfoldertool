package foldertools.utils;

import java.util.Locale;

public class Const {
	public final static String ENCODE_METHOD = "UTF-8";
	
	//public final static Locale LOCALE_DEFAULT = Locale.CHINESE;
	
	public final static int HB_HARDCODE_LEN = 24;
	public final static int HB_HARDCODE_LEN_TAILER = 8;
	
	public final static int HBDOC_HARDCODE_LEN = 24;
	public final static int HBDOC_HARDCODE_LEN_HEADER = 16;
	public final static int HBDOC_HARDCODE_LEN_TAILER = 8;
	
	public final static byte SOH = 0x01;
	public final static byte STX = 0x02;
	public final static byte ETX = 0x03;
	public final static byte EOT = 0x04;
	public final static byte NUL_ = 0x00;
	
	
	public final static byte[] HB = {0x48, 0x42};
	public final static byte[] HBEND = {0x48, 0x42, 0x45, 0x4E, 0x44};
	public final static byte[] HBEND_ALL = {0x03, 0x00, 0x48, 0x42, 0x45, 0x4E, 0x44, 0x04};
	
	public final static byte[] HBDOC = {0x48, 0x42, 0x44, 0x4F, 0x43};
	public final static byte[] DOCEND = {0x44, 0x4F, 0x43, 0x45, 0x4E, 0x44};
	public final static byte[] DOCEND_ALL = {0x03, 0x44, 0x4F, 0x43, 0x45, 0x4E, 0x44, 0x04};
	
	
	public final static String JSONKEY_ID = "id";
	public final static String JSONKEY_POSITION = "position";
	public final static String JSONKEY_FILE_LEN = "fileLen";
	public final static String JSONKEY_ACTUAL_POSITION = "actualPosition";
	public final static String JSONKEY_ACTUAL_POSITION_END = "actualPosition_end";
	public final static String JSONKEY_COMBINEFILE_PATH = "combineFilePath";
	public final static String JSONKEY_RAF_SUBFILE_POINTER = "rafSubFilePointer";
	
	public final static String COMBINEFILE_POSTFIX = "fenglincf";
}
