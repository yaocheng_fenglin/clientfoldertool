package performanceTest.cfTest;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import foldertools.ToolCombineFilesReader;

public class CFTest01 {

	/**
	 * 	Test read all files one by one
	 * 
	 */
	public static void main(String[] args) {
		String combineFilePath = "./TestMaterial/testProgram2"; 
		String subFileRootPath = "./TestMaterial/testProgram1/boost";
		
		List<String> fileList = new ArrayList<String>();
		init(subFileRootPath, fileList);
		Collections.sort(fileList);
		
		ToolCombineFilesReader reader = new ToolCombineFilesReader();
		reader.init_load_byDirectory(combineFilePath);
		
		long startTime = System.nanoTime();
		
		for(String filePath : fileList){
			reader.readWholeFile(filePath);
		}
		long endTime = System.nanoTime();
		
		long timeSpent = endTime - startTime;
		System.out.println("Time spent:" + timeSpent + " ns");
		
		System.out.println("End...Exit!");
	}

	
	private static void init(String rootPath, List<String> fileList){
		File rootFile = new File(rootPath);
		getSubFiles(rootFile, fileList);
	}

	private static void getSubFiles(File file, List<String> fileList){
		File[] curFiles = file.listFiles();
		for(File fileObj : curFiles){
			if(fileObj.isDirectory()){
				getSubFiles(fileObj, fileList);
			}else{
				fileList.add(fileObj.getPath());
			}
		}
	}
	
}
