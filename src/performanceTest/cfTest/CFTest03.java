package performanceTest.cfTest;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import foldertools.ToolCombineFilesReader;

public class CFTest03 {

	/**
	 * 	Test continue read, read 1KB every time
	 * 
	 */
	public static void main(String[] args) {
		String combineFilePath = "./TestMaterial/testProgram2"; 
		String subFileRootPath = "./TestMaterial/testProgram1/boost";
		
		List<String> fileList = new ArrayList<String>();
		init(subFileRootPath, fileList);
		Collections.sort(fileList);
		
		ToolCombineFilesReader reader = new ToolCombineFilesReader();
		reader.init_load_byDirectory(combineFilePath);
		
		long startTime = System.nanoTime();
		
		final int readLen_Max = 1024;
		for(String filePath : fileList){
			File file_one = new File(filePath);
			long remain = file_one.length();
			while(remain > 0){
				if(remain < readLen_Max){
					reader.readByRange(filePath, 0, (int)remain, true);
				}else{
					reader.readByRange(filePath, 0, readLen_Max, true);
				}
				remain = remain - 1024;
			}
		}
		long endTime = System.nanoTime();
		
		long timeSpent = endTime - startTime;
		System.out.println("Time spent:" + timeSpent + " ns");
		
		System.out.println("End...Exit!");
	}

	
	private static void init(String rootPath, List<String> fileList){
		File rootFile = new File(rootPath);
		getSubFiles(rootFile, fileList);
	}

	private static void getSubFiles(File file, List<String> fileList){
		File[] curFiles = file.listFiles();
		for(File fileObj : curFiles){
			if(fileObj.isDirectory()){
				getSubFiles(fileObj, fileList);
			}else{
				fileList.add(fileObj.getPath());
			}
		}
	}

}
