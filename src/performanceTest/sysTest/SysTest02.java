package performanceTest.sysTest;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SysTest02 {

	/**
	 * 	Test multiple thread, each thread new a Reader and reads 1000 files
	 * 
	 */
	public static void main(String[] args) {
		String subFileRootPath = "./TestMaterial/testProgram1/boost";
		
		List<String> fileList = new ArrayList<String>();
		init(subFileRootPath, fileList);
		Collections.sort(fileList);

		List<String> list_1 = new ArrayList<String>();
		for(int i=0;i<1000;i++){
			list_1.add(fileList.get(i));
		}
		List<String> list_2 = new ArrayList<String>();
		for(int j=1000;j<2000;j++){
			list_2.add(fileList.get(j));
		}
		List<String> list_3 = new ArrayList<String>();
		for(int k=2000;k<3000;k++){
			list_3.add(fileList.get(k));
		}
		
		ReadRun run1 = new ReadRun(list_1);
		ReadRun run2 = new ReadRun(list_2);
		ReadRun run3 = new ReadRun(list_3);
		
		Thread thd1 = new Thread(run1, "Test_Thread_1");
		Thread thd2 = new Thread(run2, "Test_Thread_2");
		Thread thd3 = new Thread(run3, "Test_Thread_3");
		
		thd1.start();
		thd2.start();
		thd3.start();
		
	}

	private static class ReadRun implements Runnable{
		private List<String> fileList;
		
		public ReadRun(List<String> fileList) {
			super();
			this.fileList = fileList;
		}

		@Override
		public void run() {
			long startTime = System.nanoTime();
			
			for(String filePath : fileList){
				File file = new File(filePath);
				RandomAccessFile raf = null;
				try{
					raf = new RandomAccessFile(file, "r");
					int fileLen = (int)file.length();
					byte[] b_array = new byte[fileLen];
					raf.read(b_array, 0, fileLen);
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					if(raf != null){
						try {
							raf.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
			
			long endTime = System.nanoTime();
			
			long timeSpent = endTime - startTime;
			System.out.println("Thread:" + Thread.currentThread().getName() + ", time spent:" + timeSpent + " ns");
			
		}
		
	}
	
	private static void init(String rootPath, List<String> fileList){
		File rootFile = new File(rootPath);
		getSubFiles(rootFile, fileList);
	}

	private static void getSubFiles(File file, List<String> fileList){
		File[] curFiles = file.listFiles();
		for(File fileObj : curFiles){
			if(fileObj.isDirectory()){
				getSubFiles(fileObj, fileList);
			}else{
				fileList.add(fileObj.getPath());
			}
		}
	}

}
