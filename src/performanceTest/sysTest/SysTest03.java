package performanceTest.sysTest;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SysTest03 {

	/**
	 * 	Test continue read, read 1KB every time
	 * 
	 */
	public static void main(String[] args) {
		String subFileRootPath = "./TestMaterial/testProgram1/boost";
		
		List<String> fileList = new ArrayList<String>();
		init(subFileRootPath, fileList);
		Collections.sort(fileList);

		long startTime = System.nanoTime();
		
		final int readLen_Max = 1024;
		for(String filePath : fileList){
			File file = new File(filePath);
			RandomAccessFile raf = null;
			try{
				raf = new RandomAccessFile(file, "r");
				
				long remain = file.length();
				while(remain > 0){
					if(remain < readLen_Max){
						byte[] b_array = new byte[(int) remain];
						raf.read(b_array, 0, (int)remain);
					}else{
						byte[] b_array = new byte[readLen_Max];
						raf.read(b_array, 0, readLen_Max);
					}
					remain = remain - 1024;
				}
				
				
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				if(raf != null){
					try {
						raf.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		long endTime = System.nanoTime();
		
		long timeSpent = endTime - startTime;
		System.out.println("Time spent:" + timeSpent + " ns");
		
		System.out.println("End...Exit!");
	}
	
	private static void init(String rootPath, List<String> fileList){
		File rootFile = new File(rootPath);
		getSubFiles(rootFile, fileList);
	}

	private static void getSubFiles(File file, List<String> fileList){
		File[] curFiles = file.listFiles();
		for(File fileObj : curFiles){
			if(fileObj.isDirectory()){
				getSubFiles(fileObj, fileList);
			}else{
				fileList.add(fileObj.getPath());
			}
		}
	}

}
